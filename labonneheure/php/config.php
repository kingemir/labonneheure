<?php

namespace Zotlabs\Theme;

class labonneheureConfig {

	function get_schemas() {
		$files = glob('view/theme/labonneheure/schema/*.php');

		$scheme_choices = [];

		if($files) {

			if(in_array('view/theme/labonneheure/schema/default.php', $files)) {
				$scheme_choices['---'] = t('Default');
				$scheme_choices['focus'] = t('Focus (Hubzilla default)');
			}
			else {
				$scheme_choices['---'] = t('Focus (Hubzilla default)');
			}

			foreach($files as $file) {
				$f = basename($file, ".php");
				if($f != 'default') {
					$scheme_name = $f;
					$scheme_choices[$f] = $scheme_name;
				}
			}
		}

		return $scheme_choices;
	}

	function get() {
		if(! local_channel()) { 
			return;
		}

		$arr = array();
		$arr['narrow_navbar'] = get_pconfig(local_channel(),'labonneheure', 'narrow_navbar' );
		$arr['nav_bg'] = get_pconfig(local_channel(),'labonneheure', 'nav_bg' );
		$arr['nav_icon_colour'] = get_pconfig(local_channel(),'labonneheure', 'nav_icon_colour' );
		$arr['nav_active_icon_colour'] = get_pconfig(local_channel(),'labonneheure', 'nav_active_icon_colour' );
		$arr['link_colour'] = get_pconfig(local_channel(),'labonneheure', 'link_colour' );
		$arr['banner_colour'] = get_pconfig(local_channel(),'labonneheure', 'banner_colour' );
		$arr['bgcolour'] = get_pconfig(local_channel(),'labonneheure', 'background_colour' );
		$arr['background_image'] = get_pconfig(local_channel(),'labonneheure', 'background_image' );
		$arr['item_colour'] = get_pconfig(local_channel(),'labonneheure', 'item_colour' );
		$arr['comment_item_colour'] = get_pconfig(local_channel(),'labonneheure', 'comment_item_colour' );
		$arr['font_size'] = get_pconfig(local_channel(),'labonneheure', 'font_size' );
		$arr['font_colour'] = get_pconfig(local_channel(),'labonneheure', 'font_colour' );
		$arr['radius'] = get_pconfig(local_channel(),'labonneheure', 'radius' );
		$arr['shadow'] = get_pconfig(local_channel(),'labonneheure', 'photo_shadow' );
		$arr['converse_width']=get_pconfig(local_channel(),"labonneheure","converse_width");
		$arr['top_photo']=get_pconfig(local_channel(),"labonneheure","top_photo");
		$arr['reply_photo']=get_pconfig(local_channel(),"labonneheure","reply_photo");
		$arr['advanced_theming'] = get_pconfig(local_channel(), 'labonneheure', 'advanced_theming');
		return $this->form($arr);
	}

	function post() {
		if(!local_channel()) { 
			return;
		}

		if (isset($_POST['labonneheure-settings-submit'])) {
			set_pconfig(local_channel(), 'labonneheure', 'narrow_navbar', $_POST['labonneheure_narrow_navbar']);
			set_pconfig(local_channel(), 'labonneheure', 'nav_bg', $_POST['labonneheure_nav_bg']);
			set_pconfig(local_channel(), 'labonneheure', 'nav_icon_colour', $_POST['labonneheure_nav_icon_colour']);
			set_pconfig(local_channel(), 'labonneheure', 'nav_active_icon_colour', $_POST['labonneheure_nav_active_icon_colour']);
			set_pconfig(local_channel(), 'labonneheure', 'link_colour', $_POST['labonneheure_link_colour']);
			set_pconfig(local_channel(), 'labonneheure', 'background_colour', $_POST['labonneheure_background_colour']);
			set_pconfig(local_channel(), 'labonneheure', 'banner_colour', $_POST['labonneheure_banner_colour']);
			set_pconfig(local_channel(), 'labonneheure', 'background_image', $_POST['labonneheure_background_image']);
			set_pconfig(local_channel(), 'labonneheure', 'item_colour', $_POST['labonneheure_item_colour']);
			set_pconfig(local_channel(), 'labonneheure', 'comment_item_colour', $_POST['labonneheure_comment_item_colour']);
			set_pconfig(local_channel(), 'labonneheure', 'font_size', $_POST['labonneheure_font_size']);
			set_pconfig(local_channel(), 'labonneheure', 'font_colour', $_POST['labonneheure_font_colour']);
			set_pconfig(local_channel(), 'labonneheure', 'radius', $_POST['labonneheure_radius']);
			set_pconfig(local_channel(), 'labonneheure', 'photo_shadow', $_POST['labonneheure_shadow']);
			set_pconfig(local_channel(), 'labonneheure', 'converse_width', $_POST['labonneheure_converse_width']);
			set_pconfig(local_channel(), 'labonneheure', 'top_photo', $_POST['labonneheure_top_photo']);
			set_pconfig(local_channel(), 'labonneheure', 'reply_photo', $_POST['labonneheure_reply_photo']);
			set_pconfig(local_channel(), 'labonneheure', 'advanced_theming', $_POST['labonneheure_advanced_theming']);
		}
	}

	function form($arr) {

		if(get_pconfig(local_channel(), 'labonneheure', 'advanced_theming'))
			$expert = 1;
					
	  	$o .= replace_macros(get_markup_template('theme_settings.tpl'), array(
			'$submit' => t('Submit'),
			'$baseurl' => z_root(),
			'$theme' => \App::$channel['channel_theme'],
			'$expert' => $expert,
			'$title' => t("Theme settings"),
			'$narrow_navbar' => array('labonneheure_narrow_navbar',t('Narrow navbar'),$arr['narrow_navbar'], '', array(t('No'),t('Yes'))),
			'$nav_bg' => array('labonneheure_nav_bg', t('Navigation bar background color'), $arr['nav_bg']),
			'$nav_icon_colour' => array('labonneheure_nav_icon_colour', t('Navigation bar icon color '), $arr['nav_icon_colour']),	
			'$nav_active_icon_colour' => array('labonneheure_nav_active_icon_colour', t('Navigation bar active icon color '), $arr['nav_active_icon_colour']),
			'$link_colour' => array('labonneheure_link_colour', t('Link color'), $arr['link_colour'], '', $link_colours),
			'$banner_colour' => array('labonneheure_banner_colour', t('Set font-color for banner'), $arr['banner_colour']),
			'$bgcolour' => array('labonneheure_background_colour', t('Set the background color'), $arr['bgcolour']),
			'$background_image' => array('labonneheure_background_image', t('Set the background image'), $arr['background_image']),	
			'$item_colour' => array('labonneheure_item_colour', t('Set the background color of items'), $arr['item_colour']),
			'$comment_item_colour' => array('labonneheure_comment_item_colour', t('Set the background color of comments'), $arr['comment_item_colour']),
			'$font_size' => array('labonneheure_font_size', t('Set font-size for the entire application'), $arr['font_size'], t('Examples: 1rem, 100%, 16px')),
			'$font_colour' => array('labonneheure_font_colour', t('Set font-color for posts and comments'), $arr['font_colour']),
			'$radius' => array('labonneheure_radius', t('Set radius of corners'), $arr['radius'], t('Example: 4px')),
			'$shadow' => array('labonneheure_shadow', t('Set shadow depth of photos'), $arr['shadow']),
			'$converse_width' => array('labonneheure_converse_width',t('Set maximum width of content region in pixel'),$arr['converse_width'], t('Leave empty for default width')),
			'$top_photo' => array('labonneheure_top_photo', t('Set size of conversation author photo'), $arr['top_photo']),
			'$reply_photo' => array('labonneheure_reply_photo', t('Set size of followup author photos'), $arr['reply_photo']),
			'$advanced_theming' => ['labonneheure_advanced_theming', t('Show advanced settings'), $arr['advanced_theming'], '', [t('No'), t('Yes')]]
			));

		return $o;
	}

}






