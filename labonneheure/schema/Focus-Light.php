<?php
	
	if (! $nav_bg)
		$nav_bg = "#fff";
	if (! $nav_icon_colour)
		$nav_icon_colour = "#5f4d2a";
	if (! $nav_active_icon_colour)
		$nav_active_icon_colour = "#bc833d";
	if (! $item_colour)
		$item_colour = "#B5B8FF";
	if (! $radius)
		$radius = "0px";
	if (! $banner_colour)
		$banner_colour = "#5f4d2a";
	if (! $link_colour)
		$link_colour = "#bc833d";
	if (! $bgcolour)
		$bgcolour = "#fff";
	if (! $nav_active_gradient_top)
		$nav_active_gradient_top = "#fffbb5";
	if (! $font_colour)
		$font_colour = "#5f4d2a";
